# Assumptions
- There are no running/pending SQS queues
- Both frontend and backend will run on the same AMI
- You have access to my AMI
- Filename of the image is less than 50 characters
- You have mysql set up in your environment
- Create-env.sh will create SQS topic and SNS topic.

# Usage

- Run _create-env.sh_ in the same folder as backinstall.sh and install.sh.
```sh
./create-env.sh
```

- Follow the instructions and provide all the required details - key-pair, security-group and IAM profile name.
- It's normal for the instance and load balancer to take time before being completely ready.
- Once done, you can view the webapp by visiting _Load-Balancer-DNS_ from AWS console.
- You will receive a text with a link to your transformed image in couple of minutes. 
- Copy the files from the 'local' folder to your localhost serving folder.

NOTE: Please make sure you that you have entered your credentials in jobs.php and sqs.php.
