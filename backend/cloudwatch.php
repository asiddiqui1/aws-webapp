<?php
require 'vendor/autoload.php';
 echo "hello world!\n";
$cw = new Aws\CloudWatch\CloudWatchClient([
    'version' => 'latest',
    'region'  => 'us-east-2',
    'credentials' => [
        'key'    => 'Enter key here',
        'secret' => 'Enter secret access key here',
    ],
]);
$cwresult = $cw->getMetricStatistics([
  'Dimensions' => [
      [
        'Name' => 'InstanceId',
        'Value' => 'i-08756ca2f13fe3541'
      ],
    ],
    'EndTime' => "2017-12-03T01:10:00Z", // REQUIRED
    'MetricName' => 'NetworkIn', // REQUIRED
    'Namespace' => 'AWS/EC2', // REQUIRED
    'Period' => 60, // REQUIRED
    'StartTime' => "2017-12-03T01:00:00Z", // REQUIRED
    'Statistics' => ['Maximum'],
   // 'Unit' => '
]);
print_r($cwresult);
?>