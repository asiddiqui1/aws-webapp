<?php
        require 'vendor/autoload.php';
        use Aws\Rds\RdsClient;
        $rds = new Aws\Rds\RdsClient([
                'version' => 'latest',
                'region'  => 'us-west-2',
                'credentials' => [
                    'key'    => 'Enter key here',
                    'secret' => 'Enter secret access key here',
                ],
        ]);
    
        $result = $rds->describeDBInstances([
                'DBInstanceIdentifier' => 'ahs-db-rr',
        ]);
    
        $endpoint = "";
        $endpoint = $result['DBInstances'][0]['Endpoint']['Address'];
        $link = mysqli_connect($endpoint,"afaque","afaq14093","students",3306) or die("Database connection Error " . mysqli_error($link));
            
        
        echo "<tr>";
        echo "<th>JobID</th>";
        echo "<th>Image filename</th>";
        echo "<th>Status</th>";
        echo "</tr>";
        $jobid = "";
    
        // check connection 
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        //prepare statement to get s3 url from uuid
        if (!$stmt = $link->prepare("SELECT s3raw, status, receipt from ahsrec;")) {
            echo "Prepare failed: (" . $link->errno . ") " . $link->error;
        }
        $stmt->execute();
        if (!$stmt) {
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }
        
        $stmt->bind_result($s3url, $stat, $jobid);
        //if( $row_cnt > 0) {
        while($stmt->fetch()) {
            if (!$stat)
                $curstat = "Pending";
            else if ($stat)
                $curstat = "Done";
        
            echo "<tr>";
            echo "<td>" . $jobid . "</td>";
            echo "<td>" . basename($s3url) . "</td>";
            echo "<td>" . $curstat . "</td>";
            echo "</tr>";    
        }
        
        if( empty($jobid)) {
            echo "<tr>";
            echo "<td>&nbsp</td>";
            echo "<td>Nothing to display</td>";
            echo "<td>&nbsp</td>";
            echo "</tr>";
        }
            

            //echo "\nRaw url :" . $s3url . " stat :". $stat ." jobID :" . $jobid;
        /*}
        else {
            echo "No results to display.";
        }*/
?>