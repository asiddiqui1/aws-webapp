<?php
    require 'vendor/autoload.php';
    use Aws\S3\S3Client;

    $sqs = new Aws\Sqs\SqsClient([
        'version' => 'latest',
        'region'  => 'us-west-2',
        'credentials' => [
            'key'    => 'Enter key here',
            'secret' => 'Enter secret access key here',
        ],
    ]);

    //list the SQS Queue URL
    $listQueueresult = $sqs->listQueues([]);

    $queueurl = end($listQueueresult['QueueUrls']);

    $sqsres = "";

    $sqsres = $sqs->getQueueAttributes([
        'AttributeNames' => ['ApproximateNumberOfMessages', 'ApproximateNumberOfMessagesNotVisible','DelaySeconds','MessageRetentionPeriod'],
        'QueueUrl' => $queueurl,
    ]);

    echo "<tr>";
    echo "<th>Number of messages</th>";
    echo "<th>Number of invisible messages</th>";
    echo "<th>Delay in secs</th>";
    echo "</tr>";

    if( empty($sqsres)) {
        echo "<tr>";
        echo "<td>&nbsp</td>";
        echo "<td>Nothing to display</td>";
        echo "<td>&nbsp</td>";
        echo "</tr>";
    }

    else {
        echo "<tr>";
        echo "<td>" . $sqsres['Attributes']['ApproximateNumberOfMessages'] . "</td>";
        echo "<td>" . $sqsres['Attributes']['ApproximateNumberOfMessagesNotVisible'] . "</td>";
        echo "<td>" . $sqsres['Attributes']['DelaySeconds'] . "</td>";
        echo "</tr>";
    }

?>