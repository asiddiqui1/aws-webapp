#!/bin/bash

#clone from repo
runuser -l ubuntu -c 'ssh-keyscan -t rsa github.com > ~/.ssh/known_hosts'
runuser -l ubuntu -c 'git clone git@github.com:illinoistech-itm/asiddiqui1.git asiddiqui1'

#setup environment
cp /var/www/html/vendor /home/ubuntu -r
sudo cp /home/ubuntu/asiddiqui1/ITMO-544/mp2/code/backsqs.php /home/ubuntu

#add cron job
crontab -l > phpcron
echo "* * * * * php -f /home/ubuntu/backsqs.php >> /home/ubuntu/sqs.log" >> phpcron
crontab phpcron
rm phpcron