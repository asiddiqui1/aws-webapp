#!/bin/bash

AMI="ami-bff224c7"
echo "Instance type is set to default (t2.micro)" 
echo "Image-id is :  $AMI"
read -p "Enter key pair name: " KEY 
read -p "Enter security group: " SECGRP 
echo "This security group will be used for load balancer, auto scaling frontend and backend." 
read -p "Enter IAM profile: " IAMF
echo "The 'default' security group will be used for the DB instance"
read -r -p "Are you sure you want to continue? [y/N] " response
if [[ "$response" =~ ^([nN][oO]|[nN])+$ ]]
then
    exit
else
    echo ""
fi

echo
echo "Creating SQS Queue"
#create SQS queue
aws sqs create-queue --queue-name ahs-sqs

echo
echo "Creating SNS Topic"
#create SNS topic
aws sns create-topic --name ahs-sns

#create db instance
echo
echo "Creating RDS instances"
aws rds create-db-instance --db-name students --db-instance-identifier ahs-db --db-instance-class db.t2.micro --engine mysql --master-username afaque --master-user-password afaq14093 --allocated-storage 20

echo
echo "DB instance created. Waiting for it to be available."
aws rds wait db-instance-available --db-instance-identifier ahs-db

echo
echo "DB instance now available."
echo

echo "Authorizing access."
aws ec2 authorize-security-group-ingress --group-name default --protocol tcp --port 3306 --cidr 0.0.0.0/0

echo "Creating DB read replica"
aws rds create-db-instance-read-replica --db-instance-identifier ahs-db-rr --source-db-instance-identifier ahs-db

echo
echo "DB read replica instance created. Waiting for it to be available."
aws rds wait db-instance-available --db-instance-identifier ahs-db-rr

DBID=$(aws rds describe-db-instances --db-instance-identifier ahs-db  | awk -F"\t" '$1=="ENDPOINT" {print $2;}')
echo "Creating table."
echo "USE students; CREATE TABLE ahsrec ( id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, email VARCHAR(32), phone VARCHAR(32), s3raw VARCHAR(150), s3finished VARCHAR(150), status INT(1), receipt VARCHAR(20) );" >> create-tabel.sql
mysql  -h $DBID -P 3306 -u afaque -pafaq14093 < create-tabel.sql

#create s3 buckets
echo "Creating S3 bucket 1."
aws s3 mb s3://ahs-bucket-raw --region us-west-2

echo "Creating S3 bucket 2."
aws s3 mb s3://ahs-bucket-finish --region us-west-2

#create load balancer
GRPID=$(aws ec2 describe-security-groups --group-names $SECGRP | awk '{print $5}')
echo 
echo "launching load balancer"
aws elb create-load-balancer --load-balancer-name ahs-elb --listeners "Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=80" --security-groups $GRPID --availability-zones us-west-2b

#enable sticky bits
aws elb create-lb-cookie-stickiness-policy --load-balancer-name ahs-elb --policy-name ahs-policy --cookie-expiration-period 60 

#create launch config 
echo "Creating launch configuration by assuming install.sh is present in this directory"
aws autoscaling create-launch-configuration --launch-configuration-name ahs-lc --image-id $AMI --instance-type t2.micro --key-name $KEY --security-groups $GRPID --iam-instance-profile $IAMF --user-data file://install.sh

#auto scaling groups
echo 
echo "Launching auto scaling frontend instances" 
aws autoscaling create-auto-scaling-group --auto-scaling-group-name ahs-asg --launch-configuration-name ahs-lc --min-size 3 --max-size 3 --availability-zones us-west-2b 

#backend instance
echo 
echo "Launching backend instance" 
aws ec2 run-instances --image-id $AMI --count 1 --instance-type t2.micro --key-name $KEY --iam-instance-profile Name=$IAMF --user-data file://backinstall.sh --security-groups $SECGRP

#wait for instance to be ready 
echo "Waiting for instance to return ok status (this will take time)." 
INSTIDS=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=pending" | awk -F"\t" '$1=="INSTANCES" {print $9;}')
aws ec2 wait instance-status-ok --instance-ids $INSTIDS 
echo "Instances are ready!" 

#attach load balancer to auto scaling group
echo 
echo "Attaching load balancer to auto scaling group" 
aws autoscaling attach-load-balancers --load-balancer-names ahs-elb --auto-scaling-group-name ahs-asg 

echo 
echo "Waiting for instance states to be InService (this will take time)." 
aws elb wait instance-in-service --load-balancer-name ahs-elb 
echo "Instances successfully attached!" 
