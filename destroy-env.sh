#This script deletes and removes all infrastructure created by create-env.sh

echo "Detaching load balancer"
aws autoscaling detach-load-balancers --load-balancer-names ahs-elb --auto-scaling-group-name ahs-asg

echo "Deleting load balancer"
aws elb delete-load-balancer --load-balancer-name ahs-elb

echo "Destroying auto scaling group"
aws autoscaling delete-auto-scaling-group --auto-scaling-group-name ahs-asg --force-delete

echo "Destroying launch configuration"
aws autoscaling delete-launch-configuration --launch-configuration-name ahs-lc

echo "Deleting SNS topic"
TOPIC=`aws sns list-topics | awk -F"\t" '$1=="TOPICS" {print $2;}'`
aws sns delete-topic --topic-arn "$TOPIC"

echo "Deleting SQS queue"
QUEUE=`aws sqs list-queues | awk -F"\t" '$1=="QUEUEURLS" {print $2;}'`
aws sqs delete-queue --queue-url "$QUEUE"

echo "Terminating instances"
INSTIDS=`aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" | awk -F"\t" '$1=="INSTANCES" {print $9;}'`
aws ec2 terminate-instances --instance-ids $INSTIDS

echo "Terminating DB instance"
aws rds delete-db-instance --db-instance-identifier ahs-db --skip-final-snapshot

echo "Deleting DB read-replica"
aws rds delete-db-instance --db-instance-identifier ahs-db-rr --skip-final-snapshot

echo "Deleting S3 objects"
aws s3 rb s3://ahs-bucket-raw --force
aws s3 rb s3://ahs-bucket-finish --force

echo "Done!"
