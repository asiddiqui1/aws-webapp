<?php
    require 'vendor/autoload.php';
    use Aws\S3\S3Client;

    echo "\n" . date("h:i:sa") . "\n";

    $sqs = new Aws\Sqs\SqsClient([
        'version' => 'latest',
        'region'  => 'us-west-2'
    ]);

    //list the SQS Queue URL
    $listQueueresult = $sqs->listQueues([]);

    echo "Your SQS URL is: " . end($listQueueresult['QueueUrls']) . "\n";
    $queueurl = end($listQueueresult['QueueUrls']);

    $receivemessageresult = $sqs->receiveMessage([
        'MaxNumberOfMessages' => 1,
        'QueueUrl' => $queueurl, // REQUIRED
        'VisibilityTimeout' => 60,
        'WaitTimeSeconds' => 5,
    ]);
   
    //print out content of SQS message
    $receiptHandle = $receivemessageresult['Messages'][0]['ReceiptHandle'];
    $uid = $receivemessageresult['Messages'][0]['Body'] . "\n";
    $uid = str_replace("\n", "", $uid);
    echo "The content of the message is: " . $uid . "\n";


    //connect to DB and get image url
    use Aws\Rds\RdsClient;
    $rds = new Aws\Rds\RdsClient([
            'version' => 'latest',
            'region'  => 'us-west-2',
    ]);

    $result = $rds->describeDBInstances([
            'DBInstanceIdentifier' => 'ahs-db',
    ]);

    $endpoint = "";
    $endpoint = $result['DBInstances'][0]['Endpoint']['Address'];
    $link = mysqli_connect($endpoint,"afaque","afaq14093","students",3306) or die("Database connection Error " . mysqli_error($link));

    // check connection 
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

    //prepare statement to get s3 url from uuid
    if (!$stmt = $link->prepare("SELECT s3raw FROM ahsrec WHERE receipt= ?")) {
        echo "Prepare failed: (" . $link->errno . ") " . $link->error;
    }
   
    $stmt->bind_param("s",$uid);
    $stmt->execute();
    if (!$stmt) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
    }
    $stmt->bind_result($s3url);
    $stmt->fetch();
    echo "\nRaw url :" . $s3url;
    $stmt->close();

    //grayscale conversion
    $checkimgformat=substr($s3url, -3);
    if($checkimgformat == 'png' || $checkimgformat == 'PNG'){
        $image_raw=imagecreatefrompng($s3url);
    }
    else{
        $image_raw = imagecreatefromjpeg($s3url);
    }
    if($image_raw && imagefilter($image_raw, IMG_FILTER_GRAYSCALE))
    {
        $gray_uploaddir = '/tmp';
        $gray_uploadfile = $gray_uploaddir .  basename($s3url);
        imagepng($image_raw, $gray_uploadfile);
    }
    else
    {
        echo 'Conversion to grayscale failed.';
    }

    //upload the converted file to s3

    $s3 = new Aws\S3\S3Client([
        'version' => 'latest',
        'region'  => 'us-west-2',
    ]);
    
    $resultimg = $s3->putObject(array(
        'Bucket' => 'ahs-bucket-finish',
        'Key'    =>  basename($s3url),
        'SourceFile' => $gray_uploadfile,
        'ACL' => 'public-read'
    ));
    
    $finishedurl=$resultimg['ObjectURL'];
    imagedestroy($image_raw);
    echo "\nFinished url :" . $finishedurl;
    $status = 1;


    //prepare statement to set s3finished url 
    if (!$stmt = $link->prepare("UPDATE ahsrec SET s3finished = ?, status = ? WHERE receipt = ?")) {
        echo "Prepare failed: (" . $link->errno . ") " . $link->error . "doneprep";
    }
   
    $stmt->bind_param("sis", $finishedurl, $status, $uid);
    $stmt->execute();
    if (!$stmt) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error . "doneexe";
    }
    printf("\n%d Row inserted.", $stmt->affected_rows);



    $stmt->close();
    $link->close();

    unlink($gray_uploadfile);
    
    # SNS would then send the user a text with the finsihed URL 
    # delete consumed message
    
    //SNS topic
    $sns = new Aws\Sns\SnsClient([
        'version' => 'latest',
        'region'  => 'us-west-2'
    ]);
    
    //get topic arn
    $result = $sns->listTopics([]);
    print_r ( $result['Topics']);
    $topicarn = $result['Topics'][0]['TopicArn'];
    echo "Your Topic ARN: " . $topicarn . "\n";
    
    // Publish message to subscriber
    $publishresult = $sns->publish([
        'Message' => "Hi! Your image is ready. Click on the link below to see it.\n$finishedurl", // REQUIRED
        'Subject' => 'Contact from ITMO-5444',
        'TopicArn' => $topicarn
    ]);

    //delete the message after process has completed
    $deletemessageresult = $sqs->deleteMessage([
        'QueueUrl' => $queueurl, // REQUIRED
        'ReceiptHandle' => $receiptHandle, // REQUIRED
    ]);
    
    //<!--<img src='php echo $s3url'>-->
?>