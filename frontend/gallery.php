<html>
    <head>
	  <title>Gallery</title> 
	  <!--CSS from https://codepen.io/oknoblich/pen/ELfzd -->
	  <style>
		* {
		box-sizing: border-box;
		}

		body {
		font: 14px/1 'Open Sans', sans-serif;
		color: #555;
		background: #e5e5e5;
		}

		.gallery {
		width: auto;
		margin: 5;
		padding: 25px;
		background: #fff;
		box-shadow: 0 1px 2px rgba(0,0,0,.3);
		}

		.gallery > div {
		margin: 10;
		position: relative;
		float: left;
		padding: 5px;
		}

		.gallery > div > img {
		display: block;
		width: 800px;
		transition: .1s transform;
		transform: translateZ(0); /* hack */
		}

		.gallery > div:hover {
		z-index: 1;
		}

		.gallery > div:hover > img {
		transform: scale(1.1,1.1);
		transition: .3s transform;
		}

		.cf:before, .cf:after {
		display: table;
		content: "";
		line-height: 0;
		}

		.cf:after {
		clear: both;
		}

		h1 {
		margin: 40px 0;
		font-size: 50px;
		font-weight: 300;
		text-align: center;
		}
	  </style>     
   </head>
   
   <body>
		
	 <h1>Gallery</h1>
	 <?php
		
		require 'vendor/autoload.php';
		use Aws\Rds\RdsClient;
		$client = RdsClient::factory(array(
		'region'  => 'us-west-2',
		'version' => 'latest'
		));
		
		$result = $client->describeDBInstances(array(
			'DBInstanceIdentifier' => 'ahs-db',
		));
		
		$endpoint = $result['DBInstances'][0]['Endpoint']['Address'];
		
		$dbuser = 'afaque';
		$dbpass = 'afaq14093';
		$conn = mysqli_connect($endpoint, $dbuser, $dbpass, 'students') ;
		
		if (!$conn) {
			echo "Error: Unable to connect to MySQL." . PHP_EOL;
			echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
			echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
			exit;
		}
		
		$conn->real_query("SELECT * FROM ahsrec");
		$res = $conn->use_result();
		
		
		while ($row = $res->fetch_assoc()) {
		
			echo "<div class=\"gallery cf\">";
			echo "<div>";
			echo "<img src =\" " . $row['s3raw'] . "\" />";
			echo "</div>";
			echo "<div>";
			echo "<img src =\" " . $row['s3finished'] . "\" />";
			echo "</div>";
			echo "</div>";
		
		}
   ?>
   </body>
</html>