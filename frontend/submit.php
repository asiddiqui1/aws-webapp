<?php
session_start();
ob_start();
?>

<html>
<head>
  <title>Submitting</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>
<?php
require 'vendor/autoload.php';

use Aws\S3\S3Client;

//use Imagick;

$uploaddir = '/tmp/';
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);
$upload = 0;

echo '<pre>';
if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile))
	{
		$upload = 1;
	}
else
	{
		echo '<script language="javascript">';
        echo 'alert("File upload error")';
        echo '</script>';
	}
print "</pre>";


$s3 = new Aws\S3\S3Client([
    'version' => 'latest',
    'region'  => 'us-west-2',
]);


$bucket='ahs-bucket-raw';


$result = $s3->putObject([
    'ACL' => 'public-read',
    'Bucket' => $bucket,
    'Key' =>  basename($_FILES['userfile']['name']),
    'SourceFile' => $uploadfile

]);

$url=$result['ObjectURL'];

unlink($uploadfile);


//connect to DB and insert form details
use Aws\Rds\RdsClient;
$rds = new Aws\Rds\RdsClient([
        'version' => 'latest',
        'region'  => 'us-west-2',
]);

$result = $rds->describeDBInstances([
        'DBInstanceIdentifier' => 'ahs-db',
]);

$endpoint = "";
$endpoint = $result['DBInstances'][0]['Endpoint']['Address'];
$uni = uniqid();

$link = mysqli_connect($endpoint,"afaque","afaq14093","students",3306) or die("Database connection Error " . mysqli_error($link));

// check connection 
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//prepare statement
if (!($stmt = $link->prepare("INSERT INTO ahsrec (id,email,phone,s3raw,s3finished,status,receipt) VALUES (NULL,?,?,?,?,?,?)"))) {
	echo "Prepare failed: (" . $link->errno . ") " . $link->error;
}

//initialize variables
$email = $_POST['email'];
$phone = $_POST['phone'];
$s3rawurl = $url; //  $result['ObjectURL']; from above
$s3finishedurl = "";
$status = 0;
$receipt = $uni;
$stmt->bind_param("ssssss",$email,$phone,$s3rawurl,$s3finishedurl,$status,$receipt); 

if (!$stmt->execute()) {
    echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
}

else {
    echo "<h2>Database updated!</h2>";
}

$stmt->close();

//create sqs client
$sqs = new Aws\Sqs\SqsClient([
    'version' => 'latest',
    'region'  => 'us-west-2'
]);

//list the SQS Queue URL
$listQueueresult = $sqs->listQueues([]);

echo "Your SQS URL is: " . end($listQueueresult['QueueUrls']) . "\n";
$queueurl = end($listQueueresult['QueueUrls']);

//add a message to sqs topic
$messageresult = $sqs->sendMessage([
    'DelaySeconds' => 30,
    'MessageBody' => $uni, // REQUIRED
    'QueueUrl' => $queueurl // REQUIRED
]);

echo "The messageID is: ". $messageresult['MessageId'] . "\n";

//SNS topic
$sns = new Aws\Sns\SnsClient([
    'version' => 'latest',
    'region'  => 'us-west-2'
 ]);
 
$result = $sns->listTopics([]);
$topicarn = $result['Topics'][0]['TopicArn'];
echo "Your Topic ARN: " . $topicarn;
 
$subscriberesult = $sns->subscribe([
    'Endpoint' => $phone,
    'Protocol' => 'sms', // REQUIRED
    'TopicArn' => $topicarn, // REQUIRED
]);

echo "<br/>";
echo "<h3>You have been subscribed to get notifications via sms. You will receive a text when your image is ready.</h3>";
echo "<a href=\"index.html\">Click here to go Home</a>";
?>

</body>
</html>

<?php

ob_end_flush();
?>